#ifndef _FSCK_H_
#define _FSCK_H_

#define BSIZE 512  // block size           
//extern void* bitStart; //start of bitmap

// File system super block                                                      
struct superblock {
  uint size;         // Size of file system image (blocks)                      
  uint nblocks;      // Number of data blocks                                   
  uint ninodes;      // Number of inodes.                                       
};

#define NDIRECT 12
#define NINDIRECT (BSIZE / sizeof(uint))
#define MAXFILE (NDIRECT + NINDIRECT)

// On-disk inode structure                                                      
struct dinode {
  short type;           // File type                                            
  short major;          // Major device number (T_DEV only)                     
  short minor;          // Minor device number (T_DEV only)                     
  short nlink;          // Number of links to inode in file system              
  uint size;            // Size of file (bytes)                                 
  uint addrs[NDIRECT+1];   // Data block addresses                              
};

struct dirent {
  ushort inum;
  char name[DIRSIZ];
};

#endif // _FSCK_H_       
