#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "fsck.h"
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>

int loop = 0, addrR = 0, deR = 0; //addrs[] recursion, dirent recursion
uint bitStart;
struct superblock *sb;
struct dinode *root; //root inode
struct dinode *inodeStart; //start of inode list
void *fsptr; //fs beginning
struct dinode *lf; //lost+found dinode
int nextlf = 0;
int badI; //inum with bad type
int nl[201] = {0}; //nlinks, start with nl[1] for root inode
void clear(ushort);
void setBit(ushort);
int walk(ushort, ushort);
struct dinode *getI(ushort);

int main(int argc, char **argv)
{
  //  void *fsptr; //fs beginning
  //  struct superblock *sb; //superblock
  struct stat buf;
  // int fd; //file descriptor to fs

  //open fs.img
  int fd = open(argv[1], O_RDWR);
  fstat(fd, &buf);
  int fsize = buf.st_size; //get fs size

  

void findRoot(){ //find root inode
  struct dinode *root, *tempI;
  uint block; //block numbers pointed to by inode
  int rootFound = 0;
  for(tempI = inodeStart; tempI<(struct dinode*)(inodeStart+sb->ninodes); tempI++)
    {
      if(tempI->type != 1) //not a directory
	continue;
      int ib;
      void* blockAddr;
      for(ib =0; tempI->addrs[ib]!=0; ib++) //through blocks in addrs[]
	{
	  block = tempI->addrs[ib];
	  blockAddr = (void*)(fsptr + ABLOCK(block, sb->ninodes));
	  struct dirent *de;
	  //search through dir entries in each block
	  for(de = (struct dirent*)blockAddr; de->name != NULL; de++)
	    {if(*(de->name) == 'r') 
		{rootFound = 1;
		  root = tempI;
		  printf("root name:%s \n", de->name);
		  break;
		}
	    }
	  if(rootFound == 1)
	    break;
	}
      if(rootFound == 1)
	break;
    }
}
